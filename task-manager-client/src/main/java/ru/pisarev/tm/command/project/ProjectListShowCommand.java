package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.endpoint.Project;

import java.util.List;

public class ProjectListShowCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        @Nullable List<Project> projects = serviceLocator.getProjectEndpoint().findProjectAll(getSession());
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + toString(project));
            index++;
        }
    }
}
