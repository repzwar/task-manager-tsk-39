package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.endpoint.Project;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().startProjectByName(getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
    }
}
