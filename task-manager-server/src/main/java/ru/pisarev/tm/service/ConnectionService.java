package ru.pisarev.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.repository.ISessionRepository;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.repository.IUserRepository;
import ru.pisarev.tm.exception.system.DatabaseInitException;

import javax.sql.DataSource;

public final class ConnectionService implements ru.pisarev.tm.api.service.IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new DatabaseInitException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new DatabaseInitException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new DatabaseInitException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new DatabaseInitException();

        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
